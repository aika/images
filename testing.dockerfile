# syntax=docker/dockerfile:1.2.1
FROM python:3.6-buster

# System depdendencies.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 --mount=target=/packages.txt,type=bind,source=./packages-testing.txt \
 apt-get update -q -q && \
 apt-get install --yes --no-install-recommends $(cat /packages.txt) && \
 rm -rf /tmp/* /var/tmp/*

# Python dependencies.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 --mount=target=/requirements.txt,type=bind,source=./requirements-testing.txt \
 pip3 install --no-cache-dir --upgrade --upgrade-strategy only-if-needed --requirement /requirements.txt && \
 rm -rf /tmp/* /var/tmp/*

# This argument should be provided during Docker build process.
# You can obtain it by running:
# git rev-parse HEAD
ARG testing_commit_sha
ENV TESTING_COMMIT_SHA=$testing_commit_sha
